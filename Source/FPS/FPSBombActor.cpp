// Fill out your copyright notice in the Description page of Project Settings.

#include "FPSBombActor.h"
#include "Kismet/GameplayStatics.h"
#include "TimerManager.h"
#include "Components/PrimitiveComponent.h"
#include "Materials/MaterialInstanceDynamic.h"

// Sets default values
AFPSBombActor::AFPSBombActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComp"));
	MeshComp->SetCollisionResponseToChannel(ECC_Pawn, ECR_Ignore); //set collision to blockalldynamic to collide or set collision to custom and ignore pawn to not collide
	RootComponent = MeshComp;

	ExplodeDelay = 2.0f;

	SetReplicates(true);
	SetReplicateMovement(true); //to show it at client
}

void AFPSBombActor::Explode()
{	
	UGameplayStatics::SpawnEmitterAtLocation(this, ExplosionTemplate, GetActorLocation());
	
	FCollisionObjectQueryParams QuerryParams;
	QuerryParams.AddObjectTypesToQuery(ECC_WorldDynamic);
	QuerryParams.AddObjectTypesToQuery(ECC_PhysicsBody); //objecttype actor target


	FCollisionShape CollShape;
	CollShape.SetSphere(500.0f);

	TArray<FOverlapResult> OutOverlaps;
	GetWorld()->OverlapMultiByObjectType(OutOverlaps, GetActorLocation(), FQuat::Identity, QuerryParams, CollShape);

	for (FOverlapResult Result : OutOverlaps)
	{
		UPrimitiveComponent* Overlap = Result.GetComponent();
		if (Overlap && Overlap->IsSimulatingPhysics())
		{
			UMaterialInstanceDynamic* MatInst = Overlap->CreateAndSetMaterialInstanceDynamic(0);
			if (MatInst)
			{
				MatInst->SetVectorParameterValue("Color", TargetColor);
			}
		}
	}
	
	Destroy();
}

// Called when the game starts or when spawned
void AFPSBombActor::BeginPlay()
{
	Super::BeginPlay();

	FTimerHandle Explode_TimerHandle;
	GetWorldTimerManager().SetTimer(Explode_TimerHandle, this, &AFPSBombActor::Explode, ExplodeDelay);

	 MaterialInst = MeshComp->CreateAndSetMaterialInstanceDynamic(0);
	 if (MaterialInst)
	 {
		 CurrentColor = MaterialInst->K2_GetVectorParameterValue("Color");
	 }

	 TargetColor = FLinearColor::MakeRandomColor();

}


// Called when the game starts or when spawned


// Called every frame
void AFPSBombActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);


	if (MaterialInst)
	{
		float Progress = (GetWorld()->TimeSeconds - CreationTime) / ExplodeDelay;

		FLinearColor NewColor = FLinearColor::LerpUsingHSV(CurrentColor, TargetColor, Progress);

		MaterialInst->SetVectorParameterValue("Color", NewColor);
	}

}


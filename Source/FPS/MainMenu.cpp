// Fill out your copyright notice in the Description page of Project Settings.

#include "MainMenu.h"

#include "Components/Button.h"
#include "Components/WidgetSwitcher.h"
#include "Components/EditableText.h"

bool UMainMenu::Initialize()
{
	bool Success = Super::Initialize();
	if (!Success)
		return false;

	HostButton->OnClicked.AddDynamic(this, &UMainMenu::HostServer);

	JoinServerButton->OnClicked.AddDynamic(this, &UMainMenu::JoinServerMenu);

	JoinButton->OnClicked.AddDynamic(this, &UMainMenu::Join);

	OptionButtonn->OnClicked.AddDynamic(this, &UMainMenu::Option);

	BackJoinButton->OnClicked.AddDynamic(this, &UMainMenu::BackToMenu);

	BackOptionButton->OnClicked.AddDynamic(this, &UMainMenu::BackToMenu);

	ExitButton->OnClicked.AddDynamic(this, &UMainMenu::Exit);

	return true;
}

void UMainMenu::HostServer()
{
	if (FPSInterface != nullptr)
	{
		FPSInterface->Host();
	}
}
void UMainMenu::JoinServerMenu()
{
	if (FPSInterface != nullptr)
	{
		const FString& Address = InputIP->GetText().ToString();
		FPSInterface->JoinServer(Address);
	}
}

void UMainMenu::Join()
{
	MenuSwitcher->SetActiveWidget(JoinMenu);
}

void UMainMenu::Option()
{
	MenuSwitcher->SetActiveWidget(OptionMenu);
}

void UMainMenu::BackToMenu()
{
	MenuSwitcher->SetActiveWidget(MainMenu);
}

void UMainMenu::Exit()
{
	if (FPSInterface != nullptr)
	{
		FPSInterface->ExitGame();
	}
}

void UMainMenu::SetMenuInterface(IFPSInterface* FPSInterface)
{
	this->FPSInterface = FPSInterface;
}

void UMainMenu::SetUp()
{
	this->AddToViewport();

	UWorld* World = GetWorld();

	APlayerController* PlayerController = World->GetFirstPlayerController();

	FInputModeGameAndUI InputModeData;
	InputModeData.SetWidgetToFocus(this->TakeWidget());
	InputModeData.SetLockMouseToViewportBehavior(EMouseLockMode::DoNotLock);

	PlayerController->SetInputMode(InputModeData);
	PlayerController->bShowMouseCursor = true;
}

void UMainMenu::TearDown()
{
	this->RemoveFromViewport();

	UWorld* World = GetWorld();

	APlayerController* PlayerController = World->GetFirstPlayerController();

	FInputModeGameOnly InputModeData;

	PlayerController->SetInputMode(InputModeData);
	PlayerController->bShowMouseCursor = false;
}



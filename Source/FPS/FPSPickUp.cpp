// Fill out your copyright notice in the Description page of Project Settings.

#include "FPSPickUp.h"
#include "Components/SphereComponent.h"
#include "Kismet/GameplayStatics.h"
#include "FPSCharacter.h"
#include "Components/StaticMeshComponent.h"


// Sets default values
AFPSPickUp::AFPSPickUp()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComp"));
	MeshComp->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	RootComponent = MeshComp;
	
	SphereComp = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComp"));
	SphereComp->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	SphereComp->SetCollisionResponseToAllChannels(ECR_Ignore);
	SphereComp->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);
	SphereComp->SetupAttachment(MeshComp);
	SetReplicates(true);
	SetReplicateMovement(true); //to show it at client
}

// Called when the game starts or when spawned
void AFPSPickUp::BeginPlay()
{
	Super::BeginPlay();
	
	PlayEffects();
}

void AFPSPickUp::PlayEffects()
{
	UGameplayStatics::SpawnEmitterAtLocation(this, PickUpFX, GetActorLocation());
}

// Called every frame
void AFPSPickUp::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AFPSPickUp::NotifyActorBeginOverlap(AActor* OtherActor)
{
	Super::NotifyActorBeginOverlap(OtherActor);

	PlayEffects();

	/*if (Role == ROLE_Authority)
	{*/
		AFPSCharacter* MyCharacter = Cast<AFPSCharacter>(OtherActor);
		if (MyCharacter)
		{

			MyCharacter->bIsCarryingPickUp = true;

			Destroy();
		}
	//}
}
// Fill out your copyright notice in the Description page of Project Settings.

#include "FPSGameInstance.h"

#include "UObject/ConstructorHelpers.h"

#include "Engine/Engine.h"

#include "MainMenu.h"
#include "Blueprint/UserWidget.h"

UFPSGameInstance::UFPSGameInstance(const FObjectInitializer & ObjectInitializer)
{
	ConstructorHelpers::FClassFinder<UUserWidget> MenuBPClass(TEXT("/Game/UI/WBP_EntryMenu"));
	MenuClass = MenuBPClass.Class;
}



void UFPSGameInstance::LoadMenu()
{
	Menu = CreateWidget<UMainMenu>(this, MenuClass);

	Menu->SetUp();

	Menu->SetMenuInterface(this);
}

void UFPSGameInstance::Host()
{
	/*if (Menu != nullptr)
	{
		Menu->Teardown()
	}*/

	UEngine* Engine = GetEngine();

	Engine->AddOnScreenDebugMessage(0, 2, FColor::Green, TEXT("Hosting"));

	UWorld* World = GetWorld();
	
	Menu->TearDown();

	World->ServerTravel("/Game/Map/Level1");

}

void UFPSGameInstance::ExitGame()
{
	APlayerController* PlayerController = GetFirstLocalPlayerController();
	PlayerController->ConsoleCommand("quit", true);
}

void UFPSGameInstance::JoinServer(const FString& Address)
{
	UEngine* Engine = GetEngine();
	Engine->AddOnScreenDebugMessage(0, 2, FColor::Green, TEXT("Hosting"));

	APlayerController* PlayerController = GetFirstLocalPlayerController();
	PlayerController->ClientTravel(Address, ETravelType::TRAVEL_Absolute);
}









// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "FPSInterface.h"
#include "MainMenu.generated.h"

/**
 * 
 */
UCLASS()
class FPS_API UMainMenu : public UUserWidget
{
	GENERATED_BODY()

public:
	UFUNCTION()
	void SetUp();

	UFUNCTION()
	void TearDown();

	void SetMenuInterface(IFPSInterface* FPSInterface);

	

protected:
	virtual bool Initialize();

	IFPSInterface* FPSInterface;
	


private:
	UPROPERTY(meta = (BindWidget))
	class UButton* HostButton;

	UPROPERTY(meta = (BindWidget))
	class UButton* JoinButton;

	UPROPERTY(meta = (BindWidget))
	class UButton* OptionButtonn;

	UPROPERTY(meta = (BindWidget))
	class UButton* JoinServerButton;

	UPROPERTY(meta = (BindWidget))
	class UButton* CancelButton;

	UPROPERTY(meta = (BindWidget))
	class UButton* ExitButton;

	UPROPERTY(meta = (BindWidget))
	class UButton* BackJoinButton;

	UPROPERTY(meta = (BindWidget))
	class UButton* BackOptionButton;

	UPROPERTY(meta = (BindWidget))
	class UWidgetSwitcher* MenuSwitcher;

	UPROPERTY(meta = (BindWidget))
	class UWidget* MainMenu;

	UPROPERTY(meta = (BindWidget))
	class UEditableText* InputIP;

	UPROPERTY(meta = (BindWidget))
	class UWidget* JoinMenu;

	UPROPERTY(meta = (BindWidget))
	class UWidget* OptionMenu;

	UFUNCTION()
	void HostServer();

	UFUNCTION()
	void JoinServerMenu();

	UFUNCTION()
	void Join();

	UFUNCTION()
	void Option();

	UFUNCTION()
	void Exit();

	UFUNCTION()
	void BackToMenu();


	
};
